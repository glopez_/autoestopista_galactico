﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class personajePrinicpal : MonoBehaviour
{
    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;
    
    private float velocidad;
    private float velocidadSalto;
    private bool pausado;
    private Rigidbody2D body;
    private CapsuleCollider2D capsuleCollider;
    private Vector2 movimientoDireccion;

    void Start()
    {
        velocidad = 10f;
        velocidadSalto = 20f;
        movimientoDireccion = Vector2.zero;
        capsuleCollider = GetComponent<CapsuleCollider2D>();
        // Hace referencia al componente que hemos creado en Unity de Rigidbody2D para el objeto personaje
        body = GetComponent<Rigidbody2D>();
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;

        if (!pausado)
        {
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                if (!GetComponent<SpriteRenderer>().flipX)
                    GetComponent<SpriteRenderer>().flipX = true;
                GetComponent<Animator>().SetBool("correr",true);
                movimientoDireccion.x = -2.0f;
            }
            else if ((Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow)))
            {
                if (GetComponent<SpriteRenderer>().flipX)
                    GetComponent<SpriteRenderer>().flipX = false;
                GetComponent<Animator>().SetBool("correr",true);
                movimientoDireccion.x = 2.0f;
            }
            
            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
            {
                GetComponent<Animator>().SetBool("correr",false);
                movimientoDireccion.x = 0.0f;
            }

            if ((Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && IsGrounded())
            {
                body.AddForce(new Vector2(0, velocidadSalto * Time.deltaTime * 100), ForceMode2D.Force);
            }
        }
        else
        {
            GetComponent<Animator>().SetBool("correr", false);
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }  
    }

    private void FixedUpdate()
    {
        if (!pausado)
        {
            Vector2 move = body.velocity;
            move.x = movimientoDireccion.x * velocidad * Time.deltaTime * 10;
            body.velocity = move;
        } else body.velocity = Vector2.zero;
    }

    // Comprueba si esta tocando un objeto por debajo de el
    private bool IsGrounded()
    {
        float extraHeight = 0.15f;

        RaycastHit2D rayCastHit = Physics2D.Raycast(capsuleCollider.bounds.center, Vector2.down, capsuleCollider.bounds.extents.y + extraHeight);

        return rayCastHit.collider != null;
    }

    // Cuando entra en colision con algun objeto muere
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "BalaJugador")
        {
            if (collision.gameObject.tag == "BalaEnemigo")
            {
                GameObject.Destroy(gameObject);
            }
        }
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null && menuPausa != null)
        {
            GameObject.Destroy(gameObject);
            menuPausa.transform.Find("PanelMuerte").gameObject.SetActive(true);
        }
    }
}