using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CasaBomba : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Bomba")]
    [SerializeField] private GameObject prefabBomba;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    private bool recargado;
    private bool pausado;

    void Start()
    {
        recargado = true;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        pausado = menuPausa.activeSelf;

        if (recargado && player != null && !pausado)
            disparo();
    }
    
    // Crea la bala
    void disparo()
    {
        if (transform.position.x - 8 <= player.transform.position.x && transform.position.x + 10 >= player.transform.position.x)
        {
            int direccionDisparo = 1;
            // Si esta rotada la casa cambia la posicion de la bomba
            if (transform.rotation.y != 0)
                direccionDisparo = -1;
            
            GameObject bomba = GameObject.Instantiate(prefabBomba);
            bomba.transform.position = new Vector3(transform.position.x + direccionDisparo * 0.37f, -2.10f, 0);
            bomba.tag = "BalaEnemigo";
            recargado = false;
            Invoke("recarga", 1.5f);
        }
    }

    // Recarga el disparo
    void recarga ()
    {
        recargado = true;
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }
}