using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bomba : MonoBehaviour
{
    private GameObject menuPausa;
    private float speed;
    private bool pausado;
    private Animator anim;
    private SpriteRenderer sprite;
    
    void Start()
    {
        speed = 3;
        sprite = GetComponent<SpriteRenderer>();
        pausado = false;
        menuPausa = GameObject.Find("MenuPausa").transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;
        // Y = -2.10

        if (!pausado)
        {
            // Movimiento
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }

    // Al colisionar la bomba con el suelo se destruye
    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("hola me he chocado");
        if (collision.gameObject.tag == "Bloque")
        {
            GameObject.Destroy(gameObject);
        }
    }
}