using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroduccionLvl1 : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    [Header("Camara")]
    [SerializeField] private GameObject camara;

    private float speed;
    private float fin;
    private bool texto;
    private GameObject panelPrincipal;
    private GameObject panelIntroduccion;

 
    void Start()
    {
        texto = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
        panelPrincipal = menuPausa.transform.Find("PanelPrincipal").gameObject;
        panelIntroduccion = menuPausa.transform.Find("PanelIntroduccion").gameObject;
        speed = 3;
        fin = -25f;

        menuPausa.SetActive(true);
        panelPrincipal.SetActive(false);

        PlayerPrefs.SetString("Introduccion", "true");
    }


    void Update()
    {
        if (PlayerPrefs.GetString("Reinicio") == "false")
        {
            if (!texto && player.transform.position.x < fin)
                movimientoJugador();
            else
                texto = true;

            if (texto && !panelIntroduccion.activeSelf)
            {
                player.GetComponent<Animator>().SetBool("correr", false);
                if (fin == -25f)
                    panelIntroduccion.SetActive(true);
                else
                    finIntroduccion();
            }

        } else finIntroduccion();
    }

    public void empezarJuego()
    {
        panelIntroduccion.SetActive(false);
        texto = false;
        fin = -14f;
    }

    private void movimientoJugador ()
    {
        player.transform.position += Vector3.right * speed * Time.deltaTime;
        player.GetComponent<Animator>().SetBool("correr", true);
    }

    private void finIntroduccion ()
    {
        menuPausa.SetActive(false);
        panelPrincipal.SetActive(true);
        camara.gameObject.transform.position = new Vector3(1.85f, 0, -10);
        PlayerPrefs.SetString("Introduccion", "false");
        GameObject.Destroy(gameObject);
    }
}
