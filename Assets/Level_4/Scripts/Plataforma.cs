using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plataforma : MonoBehaviour
{
    [Header("Posiciones")]
    [SerializeField] private float posIzq;
    [SerializeField] private float posDer;
    
    private float velociad;
    private int direccion;

    // Start is called before the first frame update
    void Start()
    {
        direccion = 1;
        velociad = 1;
    }

    // Update is called once per frame
    void Update()
    {
        if(transform.position.x >= posDer)
        {
            direccion = -1;
        }
        else if(transform.position.x <= posIzq)
        {
            direccion = +1;
        }

        transform.position = new Vector3(transform.position.x + velociad * direccion * Time.deltaTime, transform.position.y, transform.position.z);
    }
}
