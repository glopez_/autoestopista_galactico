using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MainCamera_4 : MonoBehaviour
{
    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    [Header("Jugador")]
    [SerializeField] private GameObject jugador;

    private float posicionInicial;
    private float posicionFinal;
    private bool pausado;

    // Start is called before the first frame update 
    void Start()
    {
        posicionInicial = 3f;
        posicionFinal = 71.12f;
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;

        if (!pausado && jugador != null)
        {
            if (Math.Round(jugador.transform.position.x) >= posicionInicial && Math.Round(jugador.transform.position.x) <= posicionFinal)
                transform.position = new Vector3(jugador.transform.position.x, transform.position.y, transform.position.z);
        }
    }
}