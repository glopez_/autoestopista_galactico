using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class JugadorPincipal : MonoBehaviour
{
    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    [Header("Vida")]
    [SerializeField] private Image StatusVida;

    [Header("Punto Guardado")]
    [SerializeField] private Animator fuego1;
    [SerializeField] private Animator fuego2;
    [SerializeField] private Animator fuego3;

    private int direccion;
    private Rigidbody2D body;
    private float velocidad = 8f;
    private float velocidadSalto = 50f;
    private bool muerto;
    private float barraVida;
    private CapsuleCollider2D capsuleCollider;
    private BoxCollider2D boxCollider;
    private SpriteRenderer sprite;
    private Vector2 moveDirection2 = Vector2.zero;
    private float posXInicio;
    private bool pausado;

    // Start is called before the first frame update
    void Start()
    {
        if (PlayerPrefs.GetString("Reinicio") == "false")
        {
            puntoGuardado(false);
        }
        else if (PlayerPrefs.GetFloat("posInicio4") != -3f)
        {
            puntoGuardado(true);
        }

        posXInicio = PlayerPrefs.GetFloat("posInicio4");
        direccion = 1;
        sprite = GetComponent<SpriteRenderer>();
        capsuleCollider = GetComponent<CapsuleCollider2D>();
        boxCollider = GetComponent<BoxCollider2D>();
        barraVida = 100;
        body = GetComponent<Rigidbody2D>();
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
        pausado = false;

        transform.position = new Vector3(posXInicio, transform.position.y, transform.position.z);
    }
    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;

        if (!muerto && !pausado)
        {
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                if (!GetComponent<SpriteRenderer>().flipX)
                {
                    GetComponent<SpriteRenderer>().flipX = true;

                }
                GetComponent<Animator>().SetBool("correr",true);
                moveDirection2.x = -2.0f;
                direccion = -1;
            } 
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                if (GetComponent<SpriteRenderer>().flipX)
                {
                    GetComponent<SpriteRenderer>().flipX = false;
                }
                GetComponent<Animator>().SetBool("correr",true);
                moveDirection2.x = 2.0f;
                direccion = 1;
            }

            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.S) || Input.GetKeyUp(KeyCode.DownArrow))
            {
                GetComponent<Animator>().SetBool("correr",false);
                GetComponent<Animator>().SetBool("agachar", false);
                capsuleCollider.isTrigger = false;
                boxCollider.isTrigger = true;
                moveDirection2.x = 0.0f;
            }

            if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && IsGrounded())
            {
                body.AddForce(new Vector2(0, velocidadSalto * Time.deltaTime * 100), ForceMode2D.Force);
            }

            if ((Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.DownArrow)) && IsGrounded())
            {
                GetComponent<Animator>().SetBool("agachar", true);
                capsuleCollider.isTrigger = true;
                boxCollider.isTrigger = false;
                moveDirection2.x = 0.0f;
            }

            barraVida = Mathf.Clamp(barraVida, 0, 100);
            StatusVida.fillAmount = barraVida / 100;
            flipXSprite();

            if (Mathf.Floor(transform.position.x) == 44)
                puntoGuardado(true);
        }

        if (!muerto && pausado)
        {
            GetComponent<Animator>().SetBool("correr", false);
            GetComponent<Animator>().SetBool("agachar", false);
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }
    }

    private void FixedUpdate()
    {
        if (!muerto)
        {
            Vector2 move2 = body.velocity;
            move2.x = moveDirection2.x * velocidad * Time.deltaTime * 10;
            body.velocity = move2;
        } else body.velocity = Vector2.zero;
    }

    // Comprueba si esta tocando un objeto
    private bool IsGrounded()
    {
        float extraHeight = 0.15f;

        RaycastHit2D rayCastHit = Physics2D.Raycast(capsuleCollider.bounds.center, Vector2.down, capsuleCollider.bounds.extents.y + extraHeight);

        return rayCastHit.collider != null && rayCastHit.collider.tag != "BalaEnemigo";
    }

    // Invierte el sprite en el Eje X dependiendo de la posicion del jugador
    private void flipXSprite()
    {
        if (direccion == 1)
            sprite.flipX = true;
        else
            sprite.flipX = false;
    }

    private void puntoGuardado(bool guardado)
    {
        if (guardado)
        {
            PlayerPrefs.SetFloat("posInicio4", 44f);
            fuego1.enabled = true;
            fuego2.enabled = true;
            fuego3.enabled = true;
        }
        else
        {
            PlayerPrefs.SetFloat("posInicio4", -3f);
            fuego1.enabled = false;
            fuego2.enabled = false;
            fuego3.enabled = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Pincho")
        {
            barraVida -= 10;
        }

        if (collision.gameObject.tag == "RecuperarVida")
        {
            barraVida += 10;
            GameObject.Destroy(collision.gameObject);  
        }

        if (barraVida <= 0)
        {
            muerto = true;
            StatusVida.fillAmount = 0;
            GameObject.Destroy(gameObject);
            menuPausa.transform.Find("PanelMuerte").gameObject.SetActive(true);
        }
    }

    private void OnBecameInvisible()
    {
        if (gameObject != null && menuPausa != null)
        {
            GameObject.Destroy(gameObject);
            menuPausa.transform.Find("PanelMuerte").gameObject.SetActive(true);
        }
    }
}
