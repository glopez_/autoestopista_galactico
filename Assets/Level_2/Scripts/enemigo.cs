using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Enemigo : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Sonido")]
    [SerializeField] private AudioSource shoot;
    [SerializeField] private AudioSource dead;    

    [Header("Bala")]
    [SerializeField] private GameObject prefabBala;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    private float speed;
    private SpriteRenderer sprite;
    private Animator anim;
    private BoxCollider2D boxCollider;
    private bool puedesDisparar;
    private bool muerto;
    private bool pausado;
    private float tiempoRecarga;

    void Start()
    {
        speed = 0.5f;
        tiempoRecarga = 0.50f;
        sprite = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();
        puedesDisparar = true;
        muerto = false;
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;

        // Si el jugador esta muerto o el enemigo no entra
        if (player != null && !muerto && !pausado)
        {
            // Movimiento hacia el jugador
            movimiento();

            // Invertir el sprite
            flipXSprite();

            // Disparo
            disparo();
        }
    }

    // El enemigo se mueve hacia la nave
    private void movimiento()
    {
        Vector3 vector = new Vector3 (
            mayor_igual_menor(transform.position.x, player.transform.position.x),
            mayor_igual_menor(transform.position.y, player.transform.position.y), 0);

        transform.position += vector * speed * Time.deltaTime;
    }

    // Invierte el sprite en el Eje X dependiendo de la posicion del jugador
    private void flipXSprite()
    {
        if (mayor_igual_menor(transform.position.x, player.transform.position.x) == -1)
            sprite.flipX = false;
        else if (mayor_igual_menor(transform.position.x, player.transform.position.x) == 1)
            sprite.flipX = true;
    }

    // Dependiendo del valor num1 (pos_enemigo) respecto num2 (pos_jugador)
    // Devuelve 1 si Mayor, -1 si Menor, 0 si Igual
    private int mayor_igual_menor(float num1, float num2)
    {
        if (num1 < num2)
            return 1;
        else if (num1 > num2)
            return -1;
        else
            return 0;
    }

    // Si el enemigo esta en la misma posicion del Eje Y disparará
    private void disparo()
    {
        if (Math.Round(transform.position.y) == Math.Round(player.transform.position.y) && puedesDisparar)
        {
            shoot.Play();
            GameObject bala = GameObject.Instantiate(prefabBala);
            bala.transform.position = new Vector3(
                transform.position.x + mayor_igual_menor(transform.position.x, player.transform.position.x) * 1.2f,
                transform.position.y,
                mayor_igual_menor(transform.position.x, player.transform.position.x));
            bala.tag = "BalaEnemigo";
            puedesDisparar = false;
            Invoke("recarga", tiempoRecarga);
        }
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        GameObject.Destroy(gameObject);
    }
    
    // Cuando entra en colision con algun objeto muere
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Enemigo")
        {
            muerto = true;
            boxCollider.isTrigger = true;
            anim.SetBool("Muerto", muerto);
            transform.localScale = new Vector3(4, 4, 0);
            dead.Play();
        }
    }

    // Cuando termine la animacion de la muerto se ejecutara esta funcion
    private void finAnimMuerto()
    {
        GameObject.Destroy(gameObject);
    }

    // Cuando dispara hay un tiempo de espera para poder volver a disparar
    private void recarga ()
    {
        puedesDisparar = true;
    }
}