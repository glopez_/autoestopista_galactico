using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroduccionLvl2 : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Vida")]
    [SerializeField] private GameObject vida;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    [Header("Camara")]
    [SerializeField] private GameObject camara;

    [Header("Estrellas")]
    [SerializeField] private SpriteRenderer estrellas;

    private float speed;
    private float fin;
    private bool texto;
    private GameObject panelPrincipal;
    private GameObject panelIntroduccion;

    // Start is called before the first frame update
    void Start()
    {
        texto = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
        panelPrincipal = menuPausa.transform.Find("PanelPrincipal").gameObject;
        panelIntroduccion = menuPausa.transform.Find("PanelIntroduccion").gameObject;
        speed = 3f;
        fin = -42f;

        vida.SetActive(false);
        menuPausa.SetActive(true);
        panelPrincipal.SetActive(false);

        PlayerPrefs.SetString("Introduccion", "true");
    }

    // Update is called once per frame
    void Update()
    {
        if (PlayerPrefs.GetString("Reinicio") == "false")
        {
            if (!texto && player.transform.position.x < fin)
                movimientoNave();
            else
                texto = true;

            if (texto && !panelIntroduccion.activeSelf)
            {
                if (fin == -42f)
                    panelIntroduccion.SetActive(true);
                else
                    finIntroduccion();
            }

            estrellas.material.mainTextureOffset += Vector2.right * (speed / 15) * Time.deltaTime;

        } else finIntroduccion();
    }

    public void empezarJuego()
    {
        panelIntroduccion.SetActive(false);
        texto = false;
        fin = -30f;
    }

    private void movimientoNave ()
    {
        player.transform.position += Vector3.right * speed * Time.deltaTime;
    }

    private void finIntroduccion ()
    {
        menuPausa.SetActive(false);
        panelPrincipal.SetActive(true);
        vida.SetActive(true);
        camara.gameObject.transform.position = new Vector3(0.55f, 0, -10);
        PlayerPrefs.SetString("Introduccion", "false");
        GameObject.Destroy(gameObject);
    }
}
