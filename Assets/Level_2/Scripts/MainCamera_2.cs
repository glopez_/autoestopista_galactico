using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCamera_2 : MonoBehaviour
{
    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    private int posicionFinal;
    private float speed;
    private bool pausado;

    // Start is called before the first frame update
    void Start()
    {
        posicionFinal = 173;
        speed = 1.7f;
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
        AudioListener.volume = PlayerPrefs.GetFloat("masterVolume") < 1 ? PlayerPrefs.GetFloat("masterVolume") : PlayerPrefs.GetFloat("masterVolume") / 10;
    }

    // Update is called once per frame
    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;
        
        // Si esta parado el nivel la musica tambien
        if (pausado)
            GetComponent<AudioSource>().Pause();
        else if (!pausado)
            GetComponent<AudioSource>().UnPause();
        
        // Si no esta en la posicion final sigue avanzando
        if (Math.Round(transform.position.x) != posicionFinal && !pausado)
            transform.position = new Vector3(transform.position.x + speed * Time.deltaTime, transform.position.y, transform.position.z);
        // Si llega al final del nivel que salga el panel de completado
        else if (Math.Round(transform.position.x) == posicionFinal)
            menuPausa.transform.Find("PanelFinal").gameObject.SetActive(true);
    }
}