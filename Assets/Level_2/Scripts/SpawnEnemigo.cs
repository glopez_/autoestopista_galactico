using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemigo : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Camara")]
    [SerializeField] private GameObject camara;

    [Header("Enemigo")]
    [SerializeField] private GameObject prefabEnemigo;

    private bool puedeCrearEnemigo;
    
    void Start()
    {
        puedeCrearEnemigo = true;
    }

    void Update()
    {
        if (player != null && puedeCrearEnemigo && PlayerPrefs.GetString("Introduccion") == "false")
        {
            if (Math.Round(player.transform.position.x) == 10)
            {
                crearEnemigo(-4, 0);
                crearEnemigo(0, 4);
            }
            else if (Math.Round(player.transform.position.x) == 30)
            {
                crearEnemigo(-4, -1);
                crearEnemigo(-1, 1);
                crearEnemigo(1, 4);
            }
            else if (Math.Round(player.transform.position.x) == 50)
            {
                crearEnemigo(-4, -2);
                crearEnemigo(-2, 0);
                crearEnemigo(0 ,2);
                crearEnemigo(2, 4);
            }
            else if (Math.Round(player.transform.position.x) == 70)
            {
                crearEnemigo(-4, -2);
                crearEnemigo(-2, 0);
                crearEnemigo(0 ,2);
                crearEnemigo(0, 2);
                crearEnemigo(2, 4);
            }
            else if (Math.Round(player.transform.position.x) == 90)
            {
                crearEnemigo(-4, -2);
                crearEnemigo(-2, -1);
                crearEnemigo(-1, 0);
                crearEnemigo(0 ,1);
                crearEnemigo(1, 2);
                crearEnemigo(2, 4);
            }
            else if (Math.Round(player.transform.position.x) == 110)
            {
                crearEnemigo(-4, -2);
                crearEnemigo(-2, -1);
                crearEnemigo(-1, 0);
                crearEnemigo(0 ,1);
                crearEnemigo(1, 2);
                crearEnemigo(2, 4);
            }
            else if (Math.Round(player.transform.position.x) == 130)
            {
                crearEnemigo(-4, -2);
                crearEnemigo(-2, -1);
                crearEnemigo(-1, 0);
                crearEnemigo(0 ,1);
                crearEnemigo(1, 2);
                crearEnemigo(2, 4);
            }
            else if (Math.Round(player.transform.position.x) == 150)
            {
                crearEnemigo(-4, -2);
                crearEnemigo(-2, -1);
                crearEnemigo(-1, 0);
                crearEnemigo(0 ,1);
                crearEnemigo(1, 2);
                crearEnemigo(2, 4);
            }
        }
    }

    public void crearEnemigo(int range1, int range2)
    {
        GameObject enemigo = GameObject.Instantiate(prefabEnemigo);
        enemigo.transform.SetParent(transform);
        enemigo.transform.position = new Vector3(camara.transform.position.x + UnityEngine.Random.Range(10, 13), UnityEngine.Random.Range(range1, range2), transform.position.z);
        enemigo.SetActive(true);
        puedeCrearEnemigo = false;
        Invoke("crearNuevoEnemigo", 1);
    }

    private void crearNuevoEnemigo()
    {
        puedeCrearEnemigo = true;
    }  
}
