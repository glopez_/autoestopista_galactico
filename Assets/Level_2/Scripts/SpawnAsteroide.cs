using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAsteroide : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Camara")]
    [SerializeField] private GameObject camara;

    [Header("Asteroide")]
    [SerializeField] private GameObject prefabAsteroide;

    private bool puedeCrearAsteroide;
    
    void Start()
    {
        Invoke("crearNuevoAsteroide", 1.5f);
    }

    void Update()
    {        
        if (puedeCrearAsteroide && camara.transform.position.x < 150 && PlayerPrefs.GetString("Introduccion") == "false")
        {
            int numAsteroides = Random.Range(3, 5);

            for (int i = 0; i < numAsteroides; i++)
                crearAsteroide();
        }
    }

    public void crearAsteroide()
    {
        GameObject asteroide = GameObject.Instantiate(prefabAsteroide);
        asteroide.transform.SetParent(transform);
        asteroide.transform.position = new Vector3(camara.transform.position.x + Random.Range(15, 20), Random.Range(-4, 4), transform.position.z);
        asteroide.SetActive(true);
        puedeCrearAsteroide = false;
        Invoke("crearNuevoAsteroide", 3);
    }

    private void crearNuevoAsteroide()
    {
        puedeCrearAsteroide = true;
    }  
}
