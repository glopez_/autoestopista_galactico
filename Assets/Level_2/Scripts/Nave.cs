using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class Nave : MonoBehaviour
{
    [Header("Sonido")]
    [SerializeField] private AudioSource shoot;
    [SerializeField] private AudioSource start;
    [SerializeField] private AudioSource dead;
    
    [Header("Vida")]
    [SerializeField] private Image barraDeVida;
    
    [Header("Bala")]
    [SerializeField] private GameObject prefabBala;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;
    
    private float speed;
    private float muertoBarra;
    private bool muerto;
    private bool recargando;
    private bool pausado;
    private float tiempoRecarga;
    private Animator anim;
    private BoxCollider2D boxCollider;

    void Start()
    {
        speed = 3;
        muertoBarra = 100;
        muerto = false;
        recargando = false;
        pausado = false;
        tiempoRecarga = 0.50f;
        anim = GetComponent<Animator>();
        boxCollider = GetComponent<BoxCollider2D>();
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;
        
        if (!muerto && !pausado)
        {
            // Movimiento
            if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D))
            {
                transform.position += Vector3.right * speed * Time.deltaTime;
            }
            
            if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A))
            {
                transform.position += Vector3.left * speed * Time.deltaTime;
            }
            
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            {
                transform.position += Vector3.down * speed * Time.deltaTime;
            }
            
            if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            {
                transform.position += Vector3.up * speed * Time.deltaTime;
            }

            // Disparo
            if (Input.GetKeyDown(KeyCode.Space) && !recargando)
            {
                shoot.Play();
                
                GameObject bala = GameObject.Instantiate(prefabBala);
                bala.transform.position = new Vector3(transform.position.x + 0.80f, transform.position.y, transform.position.z);
            
                recargando = true;
                
                Invoke("recarga", tiempoRecarga);
            }

            muertoBarra = Mathf.Clamp(muertoBarra, 0, 100);
            barraDeVida.fillAmount = muertoBarra / 100;
        }
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null && menuPausa != null)
        {
            GameObject.Destroy(gameObject);
            dead.Play();
            if (barraDeVida != null)
                barraDeVida.fillAmount = 0;
            menuPausa.transform.Find("PanelMuerte").gameObject.SetActive(true);
        }
    }

    // Cuando entra en colision con algun objeto muere
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "BalaJugador")
        {
             // barra de la vida  -10
            if (collision.gameObject.tag == "Asteroide")
            {
                muertoBarra -= 10;
            }
            // barra de la vida  -20
            if (collision.gameObject.tag == "BalaEnemigo")
            {
                muertoBarra -= 20;
                GameObject.Destroy(collision.gameObject);
            }
            if (muertoBarra <= 0)
            {
                muerto = true;
                barraDeVida.fillAmount = 0;
                boxCollider.isTrigger = true;
                anim.SetBool("Muerto", muerto);
                transform.localScale = new Vector3(4, 4, 0);
                dead.Play();
                menuPausa.transform.Find("PanelMuerte").gameObject.SetActive(true);
            }
        }
    }

    // Cuando termine la animacion de la muerto se ejecutara esta funcion
    private void finAnimMuerto ()
    {
        GameObject.Destroy(gameObject);
    }

    // Recarga el disparo
    private void recarga()
    {
        recargando = false;
    }
}
