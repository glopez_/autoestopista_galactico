using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Asteroide : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Sonido")]
    [SerializeField] private AudioSource clip;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;
    
    private int ladoRotacion;
    private int velocidadRotacion;
    private bool pausado;
    private Animator anim;
    private CapsuleCollider2D capsuleCollider;

    void Start()
    {
        if (Random.Range(0, 1) == 0)
            ladoRotacion = 1;
        else
            ladoRotacion = -1;
        velocidadRotacion = Random.Range(40, 55);
        pausado = false;
        anim = GetComponent<Animator>();
        capsuleCollider = GetComponent<CapsuleCollider2D>();
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        pausado = menuPausa.activeSelf;
        
        // Si el jugador esta muerto o el enemigo no entra
        if (player != null && !pausado) 
        {
            // Movimiento hacia el jugador
            // movimiento();
           
            transform.Rotate(0, 0, ladoRotacion * velocidadRotacion * Time.deltaTime);
        }
    }

    // El asteoride se mueve hacia la nave
    private void movimiento()
    {
        Vector3 vector = new Vector3 (
            mayor_igual_menor(transform.position.x, player.transform.position.x),
            mayor_igual_menor(transform.position.y, player.transform.position.y), 0);
    }
    
    // Dependiendo del valor num1 (pos_enemigo) respecto num2 (pos_jugador)
    // Devuelve 1 si Mayor, -1 si Menor, 0 si Igual
    private int mayor_igual_menor(float num1, float num2)
    {
        if (num1 < num2)
            return 1;
        else if (num1 > num2)
            return -1;
        else
            return 0;
    }
    
    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }

    // Cuando entra en colision con algun objeto muere
    private void OnCollisionEnter2D(Collision2D collision)
    {
        capsuleCollider.isTrigger = true;
        anim.SetBool("Destruido", true);
        clip.Play();
    }

    // Cuando termine la animacion de la muerte se ejecutara esta funcion
    private void finAnimDestruido()
    {
        GameObject.Destroy(gameObject);
    }
}
