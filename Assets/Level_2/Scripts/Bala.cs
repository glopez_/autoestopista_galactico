using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bala : MonoBehaviour
{
    private GameObject menuPausa;
    private float speed;
    private float direccion;
    private bool pausado;
    private Animator anim;
    private SpriteRenderer sprite;
    
    void Start()
    {
        speed = 7;
        direccion = 1;
        sprite = GetComponent<SpriteRenderer>();
        pausado = false;
        menuPausa = GameObject.Find("MenuPausa").transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;

        if (!pausado)
        {
            if (transform.position.z != 0)
            {
                direccion = transform.position.z;
                transform.position = new Vector3(transform.position.x, transform.position.y, 0);

                // Comprueba si el sprite tiene correcta la direcion y sino lo invierte
                flipXSprite();
            }

            // Movimiento
            transform.position += Vector3.right * direccion * speed * Time.deltaTime;
        }
    }
    

    // Invierte el sprite en el Eje X dependiendo de la posicion del jugador
    private void flipXSprite()
    {
        if (direccion == 1)
            sprite.flipX = false;
        else
            sprite.flipX = true;
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }

    // Al colisionar la bala se destruye y destruye el otro objeto
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Jugador")
        {
            // Destruye otro objeto solo si es una bala
            if ((collision.gameObject.tag == "BalaJugador" || collision.gameObject.tag == "BalaEnemigo") && collision.gameObject != null)
                GameObject.Destroy(collision.gameObject);
                
            
            // Se destruye a si mismo            
            GameObject.Destroy(gameObject);
        }
    }
}
