using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cangrejo : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject Player;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    private SpriteRenderer sprite;
    private int direccion;
    private float speed;
    private bool pausado;

    // Start is called before the first frame update
    void Start()
    {
        sprite = GetComponent<SpriteRenderer>();
        speed = 2f;
        direccion = 1;
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        pausado = menuPausa.activeSelf;
        
        if (Player != null && !pausado)
        {
            transform.position = new Vector3(transform.position.x + speed * direccion * Time.deltaTime, transform.position.y, transform.position.z);
            flipXSprite();
        }
    }

    // Invierte el sprite en el Eje X dependiendo de la posicion del jugador
    private void flipXSprite()
    {
        if (direccion != 1)
            sprite.flipX = false;
        else
            sprite.flipX = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "Jugador" && collision.gameObject.tag != "Bloque")
        {
            if (collision.gameObject.tag == "BalaEnemigo" || collision.gameObject.tag == "BalaJugador")
                GameObject.Destroy(gameObject);
            
            direccion = -direccion;
        }
    }
    
    private void OnTriggerEnter2D (Collider2D other)
    {
        direccion = -direccion;
    }

 }
