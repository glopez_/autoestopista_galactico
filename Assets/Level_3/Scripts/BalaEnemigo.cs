using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemigo : MonoBehaviour
{
    private GameObject menuPausa;
    private float speed;
    private float direccion;
    private bool pausado;
    private Animator anim;
    private SpriteRenderer sprite;
    
    void Start()
    {
        speed = 5;
        direccion = -1;
        sprite = GetComponent<SpriteRenderer>();
        pausado = false;
        menuPausa = GameObject.Find("MenuPausa").transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        pausado = menuPausa.activeSelf;

        if (!pausado)
        {
            if (transform.position.z != -1)
            {
                direccion = transform.position.z;
                transform.position = new Vector3(transform.position.x, transform.position.y, -1);
            }

            // Movimiento
            transform.position += Vector3.right * direccion * speed  * Time.deltaTime;
            flipXSprite();
        }
    }

    // Invierte el sprite en el Eje X dependiendo de la posicion del jugador
    private void flipXSprite()
    {
        if (direccion == 1)
            sprite.flipX = false;
        else
            sprite.flipX = true;
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }

    // Al colisionar la bala se destruye y destruye el otro objeto
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "jugador")
        {
            // Destruye otro objeto solo si es una bala
            if (collision.gameObject.tag == "BalaEnemigo")
                GameObject.Destroy(collision.gameObject);
                
            
            // Se destruye a si mismo            
            GameObject.Destroy(gameObject);
        }
    }
}

