using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BolaFuego : MonoBehaviour
{
    private GameObject menuPausa;
    private float speed;
    private bool pausado;
    private Animator anim;
    private SpriteRenderer sprite;
    
    void Start()
    {
        speed = 5;
        sprite = GetComponent<SpriteRenderer>();
        pausado = false;
        menuPausa = GameObject.Find("MenuPausa").transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;
        // Y = 1.85

        if (!pausado)
        {
            // Movimiento
            transform.position += Vector3.down * speed * Time.deltaTime;
        }
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }

    // Al colisionar la bomba con el suelo se destruye
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Bloque" || collision.gameObject.tag == "BalaEnemigo")
        {
            GameObject.Destroy(gameObject);
        }
    }
}