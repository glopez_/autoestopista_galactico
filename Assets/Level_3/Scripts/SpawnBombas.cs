using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnBombas : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Bomba")]
    [SerializeField] private GameObject prefabBomba;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    private bool recargado;
    private bool pausado;

    void Start()
    {
        recargado = true;
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    void Update()
    {
        pausado = menuPausa.activeSelf;

        if (recargado && player != null && !pausado)
            disparo();
    }
    
    // Crea la bala
    void disparo()
    {
        if (transform.position.x - 8 <= player.transform.position.x && transform.position.x + 10 >= player.transform.position.x)
        {            
            GameObject bomba = GameObject.Instantiate(prefabBomba);
            bomba.transform.position = new Vector3(transform.position.x, transform.position.y - 0.40f, -1);
            bomba.tag = "BalaEnemigo";
            recargado = false;
            Invoke("recarga", 2);
        }
    }

    // Recarga el disparo
    void recarga ()
    {
        recargado = true;
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }
}