using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Torre : MonoBehaviour
{    
    [Header("Jugador")]
    [SerializeField] private GameObject player;

    [Header("Bala")]
    [SerializeField] private GameObject prefabBala;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    private bool recargado;
    private int direccionDisparo;
    private bool pausado;

    // Start is called before the first frame update
    void Start()
    {
        recargado = true;
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        pausado = menuPausa.activeSelf;
        
        if (recargado & player != null && !pausado)
            disparo();
    }

    void disparo()
    {
        if (transform.position.x - 5 <= player.transform.position.x && transform.position.x + 5 >= player.transform.position.x)
        {
            if (player.transform.position.x > transform.position.x)
                direccionDisparo = +1;
            else
                direccionDisparo = -1;
            GameObject bala = GameObject.Instantiate(prefabBala);
            bala.transform.position = new Vector3(transform.position.x + direccionDisparo * 0.90f, -3.35f, direccionDisparo);
            bala.tag = "BalaEnemigo";
            recargado = false;
            Invoke("recarga", 2);
        }
    }

    // Recarga el disparo
    void recarga ()
    {
        recargado = true;
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }
}