using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;


public class PersonajePrincipal : MonoBehaviour
{

    [Header("Sonido")]
    [SerializeField] private AudioSource pincho;

    [Header("Sonido")]
    [SerializeField] private AudioSource shootEnemy;

    [Header("Sonido")]
    [SerializeField] private AudioSource dead;
    
    [Header("Sonido")]
    [SerializeField] private AudioSource shoot;

    [Header("Sonido")]
    [SerializeField] private AudioSource recargaPistola;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;
    
    [Header("Vida")]
    [SerializeField] private Image StatusVida;
    
    [Header("Bala")]
    [SerializeField] private GameObject prefabBala;

    private int direccion;
    private Rigidbody2D body;
    private float speed = 8f;
    private float jumpSpeed = 40f;
    private bool muerto;
    private float barraVida;
    private bool recargando;
    private bool pausado;
    private float tiempoRecarga;
    private CapsuleCollider2D capsuleCollider;
    private BoxCollider2D  boxCollider;

    private SpriteRenderer sprite;
    private Vector2 moveDirection2 = Vector2.zero;

    // Start is called before the first frame update
    void Start()
    {
        direccion = 1;
        recargando = false;
        pausado = false;
        sprite = GetComponent<SpriteRenderer>();
        tiempoRecarga = 0.50f;
        capsuleCollider = GetComponent<CapsuleCollider2D>();
        barraVida = 100;
        body = GetComponent<Rigidbody2D>();
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        // Si no esta activado el menuPausa no esta pausado el juego
        pausado = menuPausa.activeSelf;

        if (!muerto && !pausado)
        {
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                if (!GetComponent<SpriteRenderer>().flipX)
                {
                    GetComponent<SpriteRenderer>().flipX = true;

                }
                GetComponent<Animator>().SetBool("correr",true);
                moveDirection2.x = -2.0f;
                direccion = -1;
            } 
            else if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                if (GetComponent<SpriteRenderer>().flipX)
                {
                    GetComponent<SpriteRenderer>().flipX = false;
                }
                GetComponent<Animator>().SetBool("correr",true);
                moveDirection2.x = 2.0f;
                direccion = 1;
            }

            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.LeftArrow) || Input.GetKeyUp(KeyCode.RightArrow))
            {
                GetComponent<Animator>().SetBool("correr",false);
                moveDirection2.x = 0.0f;
            }

            if ((Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.UpArrow)) && IsGrounded())
            {
                body.AddForce(new Vector2(0, jumpSpeed * Time.deltaTime * 100), ForceMode2D.Force);
            }

            // Disparo
            if (Input.GetKeyDown(KeyCode.Space) && !recargando)
            {
                shoot.Play();
                
                GetComponent<Animator>().SetBool("disparar", true);
                GameObject bala = GameObject.Instantiate(prefabBala);
                bala.transform.position = new Vector3(transform.position.x + 0.70f * direccion, transform.position.y, -direccion);
            
                recargando = true;
                
                Invoke("recarga", tiempoRecarga);
            }

            barraVida = Mathf.Clamp(barraVida, 0, 100);
            StatusVida.fillAmount = barraVida / 100;
        }

        if (!muerto && pausado)
        {
            GetComponent<Animator>().SetBool("correr", false);
            GetComponent<Animator>().SetBool("disparar", false);
            GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
        }        
    }

    private void FixedUpdate()
    {
        if (!muerto && !pausado)
        {
            Vector2 move2 = body.velocity;
            move2.x = moveDirection2.x * speed * Time.deltaTime * 10;
            body.velocity = move2;
        } else body.velocity = Vector2.zero;
    }

    // Comprueba si esta tocando un objeto
    private bool IsGrounded()
    {
        float extraHeight = 0.15f;

        RaycastHit2D rayCastHit = Physics2D.Raycast(capsuleCollider.bounds.center, Vector2.down, capsuleCollider.bounds.extents.y + extraHeight); 

        return rayCastHit.collider != null && rayCastHit.collider.tag != "BalaEnemigo";
    }

    // Recarga el disparo
    private void recarga()
    {
        recargaPistola.Play();
        recargando = false;
    }

    // Invierte el sprite en el Eje X dependiendo de la posicion del jugador
      private void flipXSprite()
    {
        if (direccion == 1)
            sprite.flipX = false;
        else
            sprite.flipX = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag != "BalaJugador")
        {
            if (collision.gameObject.tag == "BalaEnemigo")
            {

               barraVida -= 10;
               shootEnemy.Play();
               GameObject.Destroy(collision.gameObject);
            }
            else if (collision.gameObject.tag == "Pincho")
            {
               barraVida -= 10;
               pincho.Play();
            }
            else if (collision.gameObject.tag == "Enemigo")
            {
               barraVida -= 10;
               
            }

            if (barraVida <= 0)
            {
                muerto = true;
                StatusVida.fillAmount = 0;
                GameObject.Destroy(gameObject);
                dead.Play();
                menuPausa.transform.Find("PanelMuerte").gameObject.SetActive(true);
            }            
        }
    }

    public void finDisparo() {
        GetComponent<Animator>().SetBool("disparar", false);        
    }

    private void OnBecameInvisible()
    {
        if (gameObject != null && menuPausa != null)
        {
            GameObject.Destroy(gameObject);
            menuPausa.transform.Find("PanelMuerte").gameObject.SetActive(true);
        }
    }
}
