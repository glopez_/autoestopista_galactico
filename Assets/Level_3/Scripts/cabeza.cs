using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cabeza : MonoBehaviour
{
    [Header("Jugador")]
    [SerializeField] private GameObject Player;

    [Header("Bala")]
    [SerializeField] private GameObject prefabBala;

    [Header("Menu de Pausa")]
    [SerializeField] private GameObject menuPausa;

    private bool recargado;
    private int direccionDisparo;
    private bool pausado;

    // Start is called before the first frame update
    void Start()
    {
        recargado = true;
        pausado = false;
        menuPausa = menuPausa.transform.Find("Canvas").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        pausado = menuPausa.activeSelf;

        if (recargado && Player != null && !pausado)
            disparo();
    }

    // Crea un objeto bala
    private void disparo()
    {
        if (transform.position.x - 4 <= Player.transform.position.x && transform.position.x + 4 >= Player.transform.position.x)
        {
            direccionDisparo = -1;
            GameObject bala = GameObject.Instantiate(prefabBala);
            bala.transform.position = new Vector3(transform.position.x + direccionDisparo * 0.75f, transform.position.y, direccionDisparo);
            bala.tag = "BalaEnemigo";
            recargado = false;
            Invoke("recarga", 2);
        }
    }

    // Recarga el disparo
    void recarga ()
    {
        recargado = true;
    }

    // Cuando se salga del mapa se destruira
    private void OnBecameInvisible()
    {
        if (gameObject != null)
        {
            GameObject.Destroy(gameObject);
        }
    }
}