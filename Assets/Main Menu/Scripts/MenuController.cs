using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    [Header("Menus")]
    [SerializeField] private GameObject menuPrincipalMenu;
    [SerializeField] private GameObject cargarNivelMenu;
    [SerializeField] private GameObject opcionesMenu;
    [SerializeField] private GameObject sonidoMenu;
    [SerializeField] private GameObject controlesMenu;

    [Header("Alertas")]
    [SerializeField] private GameObject nuevoJuegoAlerta;
    [SerializeField] private GameObject cargarNivelAlerta;
    [SerializeField] private GameObject confirmacionAlerta;

    [Header("Volumen")]
    [SerializeField] private Text volumenTexto;
    [SerializeField] private Slider volumenBarra;
    
    private int nivel;

    private void Start()
    {
        nivel = 0;
        reinicioValores("Audio");
    }

    public IEnumerator confirmacion()
    {
        confirmacionAlerta.SetActive(true);
        yield return new WaitForSeconds(1);
        confirmacionAlerta.SetActive(false);
    }

    //private void Update() {}

    private void ClickSound()
    {
        GetComponent<AudioSource>().Play();
    }

    public void opciones(string boton)
    {
        switch (boton)
        {
            case "Jugar":
                menuPrincipalMenu.SetActive(false);
                nuevoJuegoAlerta.SetActive(true);
                break;
            case "CargarNivel":
                menuPrincipalMenu.SetActive(false);
                cargarNivelMenu.SetActive(true);
                break;
            case "Opciones":
                menuPrincipalMenu.SetActive(false);
                opcionesMenu.SetActive(true);
                break;
            case "Controles":
                opcionesMenu.SetActive(false);
                controlesMenu.SetActive(true);
                break;
            case "Sonido":
                opcionesMenu.SetActive(false);
                sonidoMenu.SetActive(true);
                break;
            case "Nivel1":
                cargarNivelMenu.SetActive(false);
                cargarNivelAlerta.SetActive(true);
                nivel = 1;
                break;
            case "Nivel2":
                cargarNivelMenu.SetActive(false);
                cargarNivelAlerta.SetActive(true);
                nivel = 2;
                break;
            case "Nivel3":
                cargarNivelMenu.SetActive(false);
                cargarNivelAlerta.SetActive(true);
                nivel = 3;
                break;
            case "Nivel4":
                cargarNivelMenu.SetActive(false);
                cargarNivelAlerta.SetActive(true);
                nivel = 4;
                break;
            case "Salir":
                Debug.Log("Saliste");
                Application.Quit();
                break;
            default:
                Debug.Log("La opcion " + boton + " no esta codificada");
                break;
        }
    }

    public void barraVolumen()
    {
        AudioListener.volume = volumenBarra.value;
        PlayerPrefs.SetFloat("masterVolume", AudioListener.volume);
        volumenTexto.text = AudioListener.volume.ToString("0");
    }

    public void aplicarVolumen()
    {
        PlayerPrefs.SetFloat("masterVolume", AudioListener.volume);
        Debug.Log(PlayerPrefs.GetFloat("masterVolume"));
        StartCoroutine(confirmacion());
    }


    public void reinicioValores(string menuOpciones)
    {           
        if (menuOpciones == "Audio")
        {
            PlayerPrefs.SetFloat("masterVolume", 5f);
            AudioListener.volume = 5;
            volumenBarra.value = 5;
            volumenTexto.text = "5";
            aplicarVolumen();
        }
    }

    public void jugar(string boton)
    {
        if (boton == "Si")
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            PlayerPrefs.SetString("Reinicio", "false");
            Debug.Log(AudioListener.volume);
        }

        if (boton == "No")
        {
            volverMenu();
        }
    }

    public void selecionarNivel(string boton)
    {
        if (boton == "Si")
        {
            if (nivel == 0)
            {
                Debug.Log("Error en la seleccion de niveles.");
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + nivel);
                PlayerPrefs.SetString("Reinicio", "false");
            }
        }

        if (boton == "No")
        {
            volverCargarNivel();
        }
    }

    public void volverOpciones()
    {
        opcionesMenu.SetActive(true);
        sonidoMenu.SetActive(false);
        controlesMenu.SetActive(false);
        
        aplicarVolumen();
    }

    public void volverCargarNivel()
    {
        cargarNivelMenu.SetActive(true);
        cargarNivelAlerta.SetActive(false);
    }

    public void volverMenu()
    {
        menuPrincipalMenu.SetActive(true);
        cargarNivelMenu.SetActive(false);
        opcionesMenu.SetActive(false);
        sonidoMenu.SetActive(false);
        nuevoJuegoAlerta.SetActive(false);
        cargarNivelAlerta.SetActive(false);
    }
}