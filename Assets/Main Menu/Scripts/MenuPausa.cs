using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MenuPausa : MonoBehaviour
{
    private bool pausado;
    private GameObject canvas;
    private bool panelMF;
    private Slider volumen;
    private Text volumenText;

    // Start is called before the first frame update
    void Start()
    {
        pausado = false;
        panelMF = false;
        canvas = transform.Find("Canvas").gameObject;
        AudioListener.volume = PlayerPrefs.GetFloat("masterVolume");
        volumen = canvas.transform.Find("Sonido").Find("BarraVolumen").Find("Barra").gameObject.GetComponent<Slider>();
        volumenText = canvas.transform.Find("Sonido").Find("BarraVolumen").Find("TextoBarra").gameObject.GetComponent<Text>();
        ponVolumenGeneral();
    }

    // Update is called once per frame
    void Update()
    {
        if (canvas.transform.Find("PanelMuerte").gameObject.activeSelf || canvas.transform.Find("PanelFinal").gameObject.activeSelf)
        {
            panelMF = true;
            canvas.SetActive(true);
            canvas.transform.Find("PanelPrincipal").gameObject.SetActive(false);
        }

        if ((Input.GetKeyDown(KeyCode.P) || Input.GetKeyDown(KeyCode.Escape))
             && !panelMF && PlayerPrefs.GetString("Introduccion") == "false")
        {
            pausado = !pausado;
            canvas.SetActive (pausado);
            resetear();
        }
    }
    
    // Cuando se hace click a un boton del menu se llama a esta funcion
    public void opciones(string botones)
    {
        switch (botones)
        {
            case "Reanudar":
                pausado = false;
                canvas.SetActive(false);
                break;
            case "Reiniciar":
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                PlayerPrefs.SetString("Reinicio", "true");
                break;
            case "Opciones":
                canvas.transform.Find("PanelPrincipal").gameObject.SetActive(false);
                canvas.transform.Find("Opciones").gameObject.SetActive(true);
                break;
            case "Controles":
                canvas.transform.Find("Opciones").gameObject.SetActive(false);
                canvas.transform.Find("Controles").gameObject.SetActive(true);
                break;
            case "Sonido":
                canvas.transform.Find("Opciones").gameObject.SetActive(false);
                canvas.transform.Find("Sonido").gameObject.SetActive(true);
                break;
            case "SiguienteNivel":
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                break;
            case "Atras":
                canvas.transform.Find("PanelPrincipal").gameObject.SetActive(true);
                canvas.transform.Find("Opciones").gameObject.SetActive(false);
                break;
            case "AtrasOpciones":
                canvas.transform.Find("Opciones").gameObject.SetActive(true);
                canvas.transform.Find("Controles").gameObject.SetActive(false);
                canvas.transform.Find("Sonido").gameObject.SetActive(false);
                break;
            case "Salir":
                SceneManager.LoadScene(0);
                break;
            default:
                Debug.Log("No esta implementado la opcion: " + botones);
                break;
        }
    }

    public void ponVolumenGeneral()
    {
        volumen.value = PlayerPrefs.GetFloat("masterVolume");
        volumenText.text = volumen.value.ToString("0");
    }
    public void barraVolumen()
    {
        AudioListener.volume = volumen.value;
        PlayerPrefs.SetFloat("masterVolume", AudioListener.volume);
        volumenText.text = AudioListener.volume.ToString("0");
    }

    public void valorPredefinidoSonido()
    {
        PlayerPrefs.SetFloat("masterVolume", 5f);
        AudioListener.volume = 5f;
        ponVolumenGeneral();
    }
    

    public void aplicarVolumen()
    {
        PlayerPrefs.SetFloat("masterVolume", AudioListener.volume);
    }

    // Resetea el menu
    private void resetear()
    {
        canvas.transform.Find("PanelPrincipal").gameObject.SetActive(true);
        canvas.transform.Find("Opciones").gameObject.SetActive(false);
        canvas.transform.Find("Controles").gameObject.SetActive(false);
        canvas.transform.Find("Sonido").gameObject.SetActive(false);
        canvas.transform.Find("PanelMuerte").gameObject.SetActive(false);
        canvas.transform.Find("PanelFinal").gameObject.SetActive(false);
        canvas.transform.Find("PanelIntroduccion").gameObject.SetActive(false);
    }
}
