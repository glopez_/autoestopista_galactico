[![N|Solid](https://blog.hotmart.com/blog/2019/05/gerenciamento-de-tarefas-670x419.png)](https://nodesource.com/products/nsolid)
# Tareas.
 
## Desarrollo del nivel 1
**1- Escenario ambientado en la tierra.**
- [X] Texturas. | Enero |
- [X] Fondos. | Enero |
- [X] Figuras. | Enero |
- [x] Sonido cuando se realicen acciones. | Marzo |
- [X] Obstáculos. | Enero |
 
**2. Personaje principal.**
- [X] Movimientos. | Febrero |
- [X] Colisiones. | Febrero |
- [x] Sonido cuando se realicen acciones. | Marzo |
- [x] Vida de los personajes. | Febrero |          

### Desarrollo del nivel 2
**1.Escenario ambientado en el espacio**
- [X] Texturas. | Febrero |
- [X] Fondos. | Febrero |
- [X] Obstáculos. | Febrero |
- [x] Sonido cuando se realicen acciones. | Abril |

**2.Enemigos (Vogones)**
- [X] Nave espacial. | Marzo |
- [x] Vida. | Marzo |
- [X] Implementación de la IA. | Marzo |
- [X] Proyectiles. | Marzo |
- [x] Sonido cuando se realicen acciones. | Marzo|

**3.Personaje Principal**
- [X] Creación nave espacial. | Febrero |
- [X] Movimientos. | Febrero |
- [x] Vida. | Marzo |
- [X] Proyectiles. | Febrero |


### Desarrollo del nivel 3
**1.Escenario ambientado en un planeta del espacio**

- [X] Texturas. | Febrero |
- [X] Fondos. | Febrero |
- [X] Obstáculos. | Febrero |
- [X] Sonidos. |Febrero|


**2.Personaje Principal**
- [X] Movimientos. | Febrero |
- [X] Colisiones. | Febrero |
- [X] Sonido cuando se realicen acciones. | Marzo |
- [X] Vida de los personajes. | Febrero | 

- [x] Texturas. | Febrero |
- [x] Fondos. | Febrero |
- [x] Obstáculos. | Febrero |
- [X] Sonidos. | |

**3.Enemigos** 

- [x] Colisiones. | Febrero |
- [X] Implementación de la IA. | Marzo |
- [X] Vida personaje princial.| Febrero |
- [X] Vida de los enemigos. | Febrero |
- [x] Proyectiles. | Febrero |


## Jugabilidad
- [x] Patrón de comportamiento. | Febrero / Marzo |
- [x] Movimiento de la cámara. | Marzo |
- [ ] Recompensa oculta. | |
- [x] Menú.
- [x] Controles: Volumen. Pausa. Reanudar. Salir.
- [x] Banda sonora. |Abril |
- [ ] Multijugador. | Abril |
