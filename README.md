# El autoestopista galáctico

Un videojuego en 2D de plataformas.

## Comenzando 🚀

El videojuego consiste en un personaje principal, se movera por un 
escenario, en cada nivel tendra que superar una serie de obstáculos, ha medida de que vayan avanzando los niveles, sera más dificil.

### Pre-requisitos 📋


- SO: Windows 7 / 8 / 10.

- Procesador: 1.4 GHz o superior.

- Memoria: 1 GB de RAM.

- Almacenamiento: 300 MB de espacio disponible.

### Instalación 🔧

Hay un **ejecutable** que se descargara de este repositorio, ya viene directarmente instalado, sin necesidad de instalar nada en el ordenador.

## Construido con 🛠️

[Unity] (https://unity.com/es) - El motor de desarrollo de videojuego.

## Autores ✒️

* **Guillermo López Ortega**

## Licencia 📄

Este proyecto está bajo la Licencia (Tu Licencia) - mira el archivo [LICENSE.md](LICENSE.md) para detalles

